***********************************************************************************************************************************************************
											
																	My Exercise App

***********************************************************************************************************************************************************

You may need these before starting:

1. Java - JDK 7+ preferrably
2. Maven
3. Any supported IDE (ItelliJ preferred)
4. A HTTP Client like Postman
5. Access to PCF
6. Cf CLI installed
7. Git Repository Access
8. Git Bash Installed

************************************************************************************************************************************************************

Some General Information:

1. The JAR file can be found in the repository under Target folder with the name : Exercise-1.0-SNAPSHOT.jar
2. The cloud used is PCF - Pivotal Cloud Foundry.
3. BitBucket is used as the version Controlling tool. Repository created with the name : Sourabh-PlatfromEngineerPracticeExam.

*************************************************************************************************************************************************************

## STEP 1 --> To Take the clone of the codebase into the local machine

1. Open File explorer on your local machine and create a new folder.
2. Right Click and open GIT Bash, enter the following command - git clone -b <branchname> URL
	- Here branchname is the name of the branch you want to clone on your local machine -- master in our case
	- URL is the https/ssh url which you can find after clicking the CLONE button after going into the repository. - git clone   		https://sharsour@bitbucket.org/sharsour/sourabh-platfromengineerpracticeexam.git OR git clone git@bitbucket.org:sharsour/sourabh-platfromengineerpracticeexam.git for https cloning and ssh cloning respectively
3. Now you have the codebase on your local machine.

*************************************************************************************************************************************************************

## STEP 2 --> To compile the code and build a JAR

1. From the Step-1, you already have the code on your local machine.
2. Import the project as MAVEN project in any supported IDE. And Run the command mvn clean install.
3. After running the command successfully, the Target folder will be replaced, and you will an updated JAR file.
4. Now you need to push the application(JAR) to the PCF.

*************************************************************************************************************************************************************

## STEP 3 --> To push the application to the PCF

1. From the Step-2, you have an updated jar, which needs to be uploaded to PCF.
2. Open CMD and come to the destination where you have generated the target folder in the step-2.
3. Enter the command as follows :
	- cf login -a api.run.pivotal.io -u <username> -p <Password>
	- Select the corresponding org and space -- sourabhsharma-org and Dev respectively in our case
	- cf push -f manifest-dev.yml
4. Following the last command, You will notice the application will start staging in the pcf, and the logs will start coming.
5. After successful staging, a DROPLET of the code will be created which will be placed inside a CONTAINER in PCF cells.

*************************************************************************************************************************************************************

## STEP 4 --> To Test the code

1. Open any HTTP Client.
2. Select the get call and make the call with the url as follows: 
	- Positive Scenario URLs:
						- https://my-exercise-app.cfapps.io/details/123
						- https://my-exercise-app.cfapps.io/details/456
						- https://my-exercise-app.cfapps.io/alldetails
						
	- Negative Scenarios URLs:
						- https://my-exercise-app.cfapps.io/details/12
3. For every call, Notice the response recieved.

*************************************************************************************************************************************************************