package myPackage.service;

import myPackage.model.MyBean;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

@Service
public class MyService {


	public List<MyBean> retrieveAllDetails() {
		List list = new ArrayList<>();
		list.add(new MyBean(123, "TestUser", "K456789"));
		list.add(new MyBean(456, "TestUser-1", "K456789"));
		return list;
	}

	public MyBean retrieveDetail(@PathVariable int id) {
		if (id == 123)
			return new MyBean(id, "TestUser", "K456789");
		else if (id == 456)
			return new MyBean(id, "TestUser-1", "K456789");
		else
			return new MyBean(id, "Not Found", "Not Found");
	}
}
