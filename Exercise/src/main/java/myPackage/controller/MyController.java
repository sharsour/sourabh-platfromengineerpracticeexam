package myPackage.controller;

import myPackage.model.MyBean;
import myPackage.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class MyController {

	@Autowired
	private MyService myService;

	@GetMapping("/alldetails")
	public List<MyBean> retrieveAllDetails() {
		return myService.retrieveAllDetails();
	}

	@GetMapping("/details/{id}")
	public MyBean retrieveDetail(@PathVariable int id) {
		return myService.retrieveDetail(id);
	}
}


